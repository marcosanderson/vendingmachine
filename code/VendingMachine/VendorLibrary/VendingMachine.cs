﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendorLibrary
{
    public class VendingMachine
    {
        List<Item> items;
        List<IPaymentProcessor> paymentProcessors;

        private readonly uint capacity;
        private uint amount;

        public VendingMachine(uint capacity, List<IPaymentProcessor> processors = null)
        {
            this.capacity = capacity;
            amount = 0;
            items = new List<Item>((int)capacity);
            paymentProcessors = processors??new List<IPaymentProcessor>();
        }

        public bool AddItem(Item item)
        {
            if (amount + item.Amount <= this.capacity )
            {
                Item selectedItem = items.Find(p => p.ID == item.ID);
                if (selectedItem == default) items.Add(item);
                else selectedItem.Update(item.Name, item.Description, item.Amount, item.Price);

                amount += (uint)item.Amount;


                return true;
            }
            return false;
        }

        public bool RemoveItem(long id, int amount)
        {
            if (items.Count <= 0) return false;

            Item selectedItem = items.Find(p => p.ID == id);
            if (selectedItem == default) return false;
            
            if (selectedItem.Amount <= amount) {
                items.Remove(selectedItem);
            }
            else{
                selectedItem.Amount -= amount;
            }
            return true;
        }

        public Item ChooseItem(long id)
        {
            return items.Find(p => p.ID == id);
        }

        public IPaymentProcessor ChoosePayment(long id)
        {
            return paymentProcessors.Find(p => p.ID == id);
        }

        public bool Checkout(Item selectedItem, IPaymentProcessor processor, out double change)
        {
            change = 0;
            if (selectedItem == null || processor == null) return false;
            if (!processor.Validate()) return false;
            if (processor.Charge(selectedItem.Price, out change)){
                RemoveItem(selectedItem.ID, 1);
                return true;
            }
            return false;
        }
    }
}
