﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendorLibrary
{
    public abstract class Item
    {
        private readonly long id;
        private string name;
        private string description;
        private int amount;
        private double price;

        public Item(int id, string name, string description, int amount, double price)
        {
            this.id = id;
            Name = name;
            Description = description;
            Amount = amount;
            Price = price;
        }

        public long ID { get => this.id; }
        public int Amount {
            get { return this.amount; }
            set { this.amount = value; }
        }
        public string Name { get => this.name; private set => this.name = value; }
        public string Description { get => this.description; private set => this.description = value; }
        public double Price { get => this.price; private set => this.price = value; }

        internal void Update(string name, string description, int amount, double price)
        {
            Name = name;
            Description = description;
            Amount = amount;
            Price = price;
        }
    }
}
