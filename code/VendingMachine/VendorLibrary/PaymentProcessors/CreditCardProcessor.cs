﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendorLibrary.PaymentProcessors
{
    public class CreditCardProcessor : IPaymentProcessor
    {
        public long ID { get => 1;}

        public double Credit { get; private set; }

        public readonly string Number;
        public readonly string Name;
        public readonly string Flag;
        public readonly string Csv;

        public CreditCardProcessor(string number, string name, string flag, string csv)
        {
            Number = number;
            Name = name;
            Flag = flag;
            Csv = csv;

            //Check API to see the cards balance
            Credit = 100;
        }

        public bool Charge(double total, out double change)
        {
            change = 0;
            if (total > Credit) return false;
            change = Credit - total;
            Credit -= total;
            return true;
        }

        public bool Validate()
        {
            //performs check to see if the card is valid
            return Number.Length == 16 && long.TryParse(Number, out long number);
        }
    }
}
