﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendorLibrary.PaymentProcessors
{
    public class CashProcessor : IPaymentProcessor
    {
        public double Credit { get; private set; }
        public long ID { get => 3; }

        public CashProcessor(double amount)
        {
            Credit = amount;
        }

        public bool Charge(double total, out double change)
        {

            change = 0;
            if (total > Credit) return false;
            change = Credit - total;
            Credit -= total;
            return true;
        }

        public bool Validate()
        {
            //performs checks whether the fiat money is valid

            //in this case if the amount is less then 20 dollars
            return Credit < 20;
        }
    }
}
