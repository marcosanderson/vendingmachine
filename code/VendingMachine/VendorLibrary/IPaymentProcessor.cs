﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendorLibrary
{
    public interface IPaymentProcessor
    {
        long ID { get; }
        bool Validate();

        bool Charge(double total, out double change);

    }
}
