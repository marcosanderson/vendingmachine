﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendorLibrary.Items
{
    public class KitKat : Item
    {
        public KitKat(int amount, double price) : base(1, "KitKat", "Chocolicious", amount, price)
        { }
    }
}
