﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendorLibrary.Items
{
    public class Snack : Item
    {
        public Snack(int amount, double price) : base(2, "Snack", "Choco", amount, price) { }
    }
}
