﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using VendorLibrary;
using VendorLibrary.Items;
using VendorLibrary.PaymentProcessors;

namespace Vendor.Tests
{
    [TestClass]
    public class VendingMachineTests
    {

        #region Test_AddItem

        [TestMethod]
        public void AddItem_WhenEmpty_ReturnsTrue()
        {
            //Arrange
            uint capacity = 50;
            VendingMachine machine = new VendingMachine(capacity);
            //Act
            bool output = machine.AddItem(new KitKat(50, 1.25));

            //Assert
            Assert.IsTrue(output);
        }

        [TestMethod]
        public void AddItem_WhenFull_ReturnsFalse()
        {
            //Arrange
            uint capacity = 10;
            VendingMachine machine = new VendingMachine(capacity);
            machine.AddItem(new KitKat((int)capacity, 1.25));

            //Act
            bool output = machine.AddItem(new KitKat(50, 1.25));

            //Assert
            Assert.IsFalse(output);
        }

        [TestMethod]
        public void AddItem_WhenFindAndFull_ReturnsFalse()
        {
            //Arrange
            uint capacity = 10;
            VendingMachine machine = new VendingMachine(capacity);
            for (int i = 0; i < capacity / 2; i++)
            {
                machine.AddItem(new KitKat(2, 1.25));
            }

            //Act
            bool output = machine.AddItem(new KitKat(5, 1.25));

            //Assert
            Assert.IsFalse(output);
        }
        [TestMethod]
        public void AddItem_WhenFind_ReturnsTrue()
        {
            //Arrange
            uint capacity = 10;
            VendingMachine machine = new VendingMachine(capacity);
            for (int i = 0; i < capacity/2; i++)
            {
                machine.AddItem(new KitKat(1, 1.25));
            }

            //Act
            bool output = machine.AddItem(new KitKat(1, 1.25));

            //Assert
            Assert.IsTrue(output);
        }

        #endregion


        #region Test_RemoveItem

        [TestMethod]
        public void RemoveItem_WhenEmpty_ReturnsFalse()
        {
            //Arrange
            VendingMachine machine = new VendingMachine(10);
            //Act
            long id = 1;
            bool output = machine.RemoveItem(id, 5);
            //Assert
            Assert.IsFalse(output);
        }

        [TestMethod]
        public void RemoveItem_WhenNotFound_ReturnsFalse()
        {
            //Arrange
            VendingMachine machine = new VendingMachine(5);
            machine.AddItem(new KitKat(3, 2.35));
            machine.AddItem(new Snack(2, 0.36));
            //Act
            long id = 16;
            bool output = machine.RemoveItem(id, 10);
            //Assert
            Assert.IsFalse(output);
        }

        [TestMethod]
        public void RemoveItem_WhenFoundAndNotEnough_ReturnsTrue()
        {
            //Arrange
            VendingMachine machine = new VendingMachine(5);
            machine.AddItem(new KitKat(3, 2.35));
            machine.AddItem(new Snack(2, 0.36));
            //Act
            long id = 1;
            bool output = machine.RemoveItem(id, 10);
            //Assert
            Assert.IsTrue(output);
        }

        [TestMethod]
        public void RemoveItem_WhenFoundAndEnough_ReturnsTrue()
        {
            //Arrange
            VendingMachine machine = new VendingMachine(5);
            machine.AddItem(new KitKat(3, 2.35));
            machine.AddItem(new Snack(2, 0.36));
            //Act
            long id = 1;
            bool output = machine.RemoveItem(id, 2);
            //Assert
            Assert.IsTrue(output);
        }

        #endregion


        #region Test_ChooseItem
        [TestMethod]
        public void ChooseItem_WhenNotFound_ReturnsNull()
        {
            //Arrange
            VendingMachine machine = new VendingMachine(10);
            machine.AddItem(new KitKat(5, 2));
            machine.AddItem(new Snack(2, 3.5));
            machine.AddItem(new KitKat(5, 2));
            //Act
            long id = 4;
            Item selectedItem = machine.ChooseItem(id);

            //Assert
            Assert.AreEqual(null, selectedItem);
        }
        [TestMethod]
        public void ChooseItem_WhenFound_ReturnsItem()
        {
            //Arrange
            VendingMachine machine = new VendingMachine(10);
            var item1 = new KitKat(5, 2);
            var item2 = new Snack(2, 3.5);
            var item3 = new KitKat(5, 2);
            machine.AddItem(item1);
            machine.AddItem(item2);
            machine.AddItem(item3);
            //Act
            long id = 2;
            Item selectedItem = machine.ChooseItem(id);

            //Assert
            Assert.AreEqual(item2, selectedItem);
        }
        #endregion


        #region Test_ChoosePaymentType
        [TestMethod]
        public void ChoosePaymentType_WhenNotFound_ReturnsNull()
        {
            //Arrange
            IPaymentProcessor creditCardProc = new CreditCardProcessor("1234567890123456", "Mark A", "visa", "123");
            IPaymentProcessor debitCardProc = new DebitCardProcessor("1234567890123456", "Mark A");
            IPaymentProcessor cashProc = new CashProcessor(20);

            VendingMachine machine = new VendingMachine(10, new List<IPaymentProcessor>() {
                creditCardProc, debitCardProc, cashProc    
            });

            //Act
            long id = 20;
            IPaymentProcessor selectedItem = machine.ChoosePayment(id);

            //Assert
            Assert.AreEqual(null, selectedItem);
        }
        [TestMethod]
        public void ChoosePaymentType_WhenFound_ReturnsPaymentType()
        {
            //Arrange
            IPaymentProcessor creditCardProc = new CreditCardProcessor("1234567890123456", "Mark A", "visa", "123");
            IPaymentProcessor debitCardProc = new DebitCardProcessor("1234567890123456", "Mark A");
            IPaymentProcessor cashProc = new CashProcessor(20);

            VendingMachine machine = new VendingMachine(10, new List<IPaymentProcessor>() {
                creditCardProc, debitCardProc, cashProc
            });

            //Act
            long id = 3;
            IPaymentProcessor selectedItem = machine.ChoosePayment(id);

            //Assert
            Assert.AreEqual(cashProc, selectedItem);
        }

        #endregion

        #region Checkout

        [TestMethod]
        public void Checkout_WhenNotValidItem_ReturnsFalse()
        {
            //Arrange
            IPaymentProcessor creditCardProc = new CreditCardProcessor("1234567890123456", "Mark A", "visa", "123");
            IPaymentProcessor debitCardProc = new DebitCardProcessor("1234567890123456", "Mark A");
            IPaymentProcessor cashProc = new CashProcessor(20);

            VendingMachine machine = new VendingMachine(50, new List<IPaymentProcessor>() {
                creditCardProc, debitCardProc, cashProc
            });

            machine.AddItem(new KitKat(25, 2));
            machine.AddItem(new Snack(20, 3.5));

            long idItem = 5;
            Item selectedItem = machine.ChooseItem(idItem);

            IPaymentProcessor processor = machine.ChoosePayment(2);

            //Act
            bool output = machine.Checkout(selectedItem, processor, out double change);

            //Assert
            Assert.IsFalse(output);
        }
        [TestMethod]
        public void Checkout_WhenNotValidProcessor_ReturnsFalse()
        {
            //Arrange
            IPaymentProcessor creditCardProc = new CreditCardProcessor("1234567890123456", "Mark A", "visa", "123");
            IPaymentProcessor debitCardProc = new DebitCardProcessor("1234567890123456", "Mark A");
            IPaymentProcessor cashProc = new CashProcessor(20);

            VendingMachine machine = new VendingMachine(50, new List<IPaymentProcessor>() {
                creditCardProc, debitCardProc, cashProc
            });

            machine.AddItem(new KitKat(25, 2));
            machine.AddItem(new Snack(20, 3.5));

            Item selectedItem = machine.ChooseItem(1);
            IPaymentProcessor processor = machine.ChoosePayment(15);

            //Act
            bool output = machine.Checkout(selectedItem, processor, out double change);

            //Assert
            Assert.IsFalse(output);
        }

        [TestMethod]
        public void Checkout_WhenCashPaymentNotValid_ReturnsFalse()
        {
            //Arrange
            IPaymentProcessor creditCardProc = new CreditCardProcessor("1234567890123456", "Mark A", "visa", "123");
            IPaymentProcessor debitCardProc = new DebitCardProcessor("1234567890123456", "Mark A");
            IPaymentProcessor cashProc = new CashProcessor(20);

            VendingMachine machine = new VendingMachine(50, new List<IPaymentProcessor>() {
                creditCardProc, debitCardProc, cashProc
            });

            machine.AddItem(new KitKat(25, 2));
            machine.AddItem(new Snack(20, 3.5));

            Item selectedItem = machine.ChooseItem(1);
            IPaymentProcessor processor = machine.ChoosePayment(3);

            //Act
            bool output = machine.Checkout(selectedItem, processor, out double change);

            //Assert
            Assert.IsFalse(output);
        }
        [TestMethod]
        public void Checkout_WhenCashPaymentInsuficient_ReturnsFalse()
        {
            //Arrange
            IPaymentProcessor creditCardProc = new CreditCardProcessor("1234567890123456", "Mark A", "visa", "123");
            IPaymentProcessor debitCardProc = new DebitCardProcessor("1234567890123456", "Mark A");
            IPaymentProcessor cashProc = new CashProcessor(10);

            VendingMachine machine = new VendingMachine(50, new List<IPaymentProcessor>() {
                creditCardProc, debitCardProc, cashProc
            });

            machine.AddItem(new KitKat(25, 15));
            machine.AddItem(new Snack(20, 3.5));

            Item selectedItem = machine.ChooseItem(1);
            IPaymentProcessor processor = machine.ChoosePayment(3);

            //Act
            bool output = machine.Checkout(selectedItem, processor, out double change);

            //Assert
            Assert.IsFalse(output);
        }
        [DataTestMethod]
        [DataRow(1, 2, 1)]
        [DataRow(1, 5, 4)]
        [DataRow(10, 15, 5)]
        [DataRow(1, 1, 0)]
        public void Checkout_WhenCashPaymentSuficient_ReturnsTrueAndChange(double cost, double totalPay, double expectedChange)
        {
            //Arrange
            IPaymentProcessor creditCardProc = new CreditCardProcessor("1234567890123456", "Mark A", "visa", "123");
            IPaymentProcessor debitCardProc = new DebitCardProcessor("1234567890123456", "Mark A");
            IPaymentProcessor cashProc = new CashProcessor(totalPay);

            VendingMachine machine = new VendingMachine(50, new List<IPaymentProcessor>() {
                creditCardProc, debitCardProc, cashProc
            });

            machine.AddItem(new KitKat(25, cost));
            machine.AddItem(new Snack(20, 3.5));

            Item selectedItem = machine.ChooseItem(1);
            IPaymentProcessor processor = machine.ChoosePayment(3);

            //Act
            bool output = machine.Checkout(selectedItem, processor, out double change);

            //Assert
            Assert.IsTrue(output);
            Assert.AreEqual(expectedChange, change);
        }

        #endregion
    }
}
